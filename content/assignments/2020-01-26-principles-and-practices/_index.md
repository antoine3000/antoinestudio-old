---
title: Principles and practices
---

For the next six months (from January to June 2020), I'll follow the [Fab Academy](https://fabacademy.org/) at [Fab Lab Barcelona](https://fablabbcn.org/). The FabAcademy is an intensive program to learn how to design and prototype projects using digital fabrication tools and machine, such as: computer-controlled cutting, electronics, 3D scanning and printing, electronics design,computer-controlled machining, embedded programming, molding and casting, networking and communications, mechanical design, interface and application programming and machine design.

I've a background in graphic & web design and web development and I love to imagine and build experiences for the common using open-source tools. But working in the web industry, I got a little bored of building things from a computer to another. I started to feel the need of working with tangible materials. I want to learn how to repair the old instead of creating the new, to think about our energy consumption, to (re)think about how we use our tools in the everyday life and what it means, and to dedicate my practice to nature.

# Final project proposal

As part of the academy, I was asked to think about a personal project that represents all the techniques I will learn. I have had something in mind for some time and this academy can offer me the tools to do my research. Let me explain it to you.

I am fascinated by nature, by all the plants and their survival mechanisms, all the means they find to perpetuate their species. I would like to understand them better and to invite more people to take a closer look, to take more interest in this absolutely incredible ecosystem in which we all live.

What if we could have a tangible way to visualize how plants communicate, a way to understand them more easily? Would we respect them more?

![first-sketch](first-sketch.jpg)


## Listen to what the plants have to say

Plants communicate as we do. But it's hardly noticeable for us because we don't send and receive signals at the same speed as they do. This is why I would like to build a kind of machine that can help any plant to express its needs at a speed that we can actually perceive and understand. Or should I reformulate in a less human-centric way: a machine that can help humans understand what the plants have to say.

## A machine that helps us understand plants 

The machine I have in mind is able to get the data from a plant [temperature, light level, air quality, humidity level, nutrient level, vibrations, ?] and transform it into something visual or audible. The machine itself could be small, like a tool that helps humans understand plants in everyday life, or large, as an art installation that invites the public to join a piece of nature, to watch and listen to what it has to say.


1. Obtain environmental data (humidity, light, temperature)
2. Compare them with the electrical signal emitted by the plant
3. Find where the links between these different data are


### Breathing

Breathing (or respiration) is a very common process between plants and animals. This is the kind of movement I can use to represent how the plant feels in relation to its environment.

![final-project-drawings](final-project-drawings.png)


### How to simulate breathing

- Use a pump to inject and suck the air contained in a flexible and hermetic material
- Use rods, powered by motors, which push and release a flexible material, to deform it

### System/machine

A system/machine that can "plugged" to any plant, from trees to houseplants.







